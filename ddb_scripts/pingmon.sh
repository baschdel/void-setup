host="$1"
object_name="$2"
idle_time="$3"

help() {
cat <<EOF
Usage:
	ddb_pingmon <host> <object_name> [<idle_time>]

Ping <host> and exports an online/offline status as a ddb object.
Also emits a ping signal on the object just before sending the ping.
Waits <idle_time> seconds after reveiving a ping or timing out
before pinging again, defaults to 30 seconds.
EOF
exit
}

[ -z "$host" ] && help
[ -z "$object_name" ] && help
[ -z "$idle_time" ] && idle_time=30

echo "> $object_name type pingmon"
echo "> $object_name hostname $host"

while true; do
	echo "s $object_name ping"
	if ping -W 3 -c 1 "$host" >/dev/null 2>/dev/null
	then
		echo "> $object_name status online"
	else
		echo "> $object_name status offline"
	fi
	sleep "$idle_time"
done
