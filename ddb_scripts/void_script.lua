-- This is a script to be used with ddb_logic
-- It is named after the host it is supposed to run on

local KeepTimer = require("keep_timer").KeepTimer

helpers = {
	amp_timer = KeepTimer(30), -- turn amps off after 30s of muted audio
	amp_js_timer = KeepTimer(10), -- amp_J needs a few seconds to start
	last_amp_timer_output = false,
	tv_timer = KeepTimer(30*60), -- Advertisement breaks can be long …
	recv_timer = KeepTimer(30*60), 
}

function do_logic(inputs, outputs, helpers)
	outputs.audio_ad = inputs.ad_online and inputs.audio_ad
	outputs.audio_tv = inputs.audio_tv
	outputs.audio_aux = inputs.audio_aux
	helpers.amp_timer:set_input_value(outputs.audio_ad or outputs.audio_aux or outputs.audio_tv)
	if inputs.amp_mode == "JS" then
		if helpers.amp_timer.output and not helpers.last_amp_timer_output then
			helpers.amp_js_timer:set_input_value(true)
			helpers.amp_js_timer:set_input_value(false)
		end
		outputs.amp_T = helpers.amp_js_timer.output
		outputs.amp_J = helpers.amp_timer.output
	elseif inputs.amp_mode == "J" then
		outputs.amp_T = false
		outputs.amp_J = helpers.amp_timer.output
	elseif inputs.amp_mode == "T" then
		outputs.amp_T = helpers.amp_timer.output
		outputs.amp_J = false
	elseif inputs.amp_mode == "JT" then
		outputs.amp_T = helpers.amp_timer.output
		outputs.amp_J = helpers.amp_timer.output
	end
	helpers.last_amp_timer_output = helpers.amp_timer.output
	outputs.screen_L = inputs.ad_online
	outputs.screen_M = inputs.ad_online
	helpers.tv_timer:set_input_value(inputs.audio_tv)
	helpers.tv_timer:set_off_value(inputs.ad_online)
	outputs.screen_T = helpers.tv_timer.output or inputs.ad_online
	helpers.recv_timer:set_input_value(inputs.audio_tv)
	-- only turn the receiver on when the tv is on
	outputs.receiver = helpers.recv_timer.output and outputs.screen_T
end

local function tick(inputs, outputs, helpers)
	local update_logic = false
	for _,helper in pairs(helpers) do
		if type(helper) == "table" then	
			if type(helper.tick) == "function" then
				update_logic = update_logic or helper:tick()
			end
		end
	end
	if update_logic then
		do_logic(inputs, outputs, helpers)
	end
end

local function off_signal(inputs, outputs, helpers)
	helpers.amp_timer:reset()
	helpers.amp_js_timer:reset()
	helpers.tv_timer:reset()
	helpers.recv_timer:reset()
	do_logic(inputs, outputs, helpers)
end

local function on_off_to_bool(i)
	if i then
		if i:match("^on") then
			return true
		end
	end
	return false
end

local function to_on_off_bool(i)
	if i then
		return "on"
	else
		return "off"
	end
end

add_signal("requests off", off_signal)
add_signal("pingmon-amber ping", tick) --Use the pingsignal as timer

add_input("ad_online", "pingmon-amber status", on_off_to_bool, false)
add_input("audio_ad", "requests audio-amber", on_off_to_bool, false)
add_input("audio_tv", "requests audio-tv", on_off_to_bool, false)
add_input("audio_aux", "requests audio-aux", on_off_to_bool, false)
add_input("amp_mode", "requests amp_mode", tostring, "J")

add_output("audio_ad", "asw x", to_on_off_bool, false)
add_output("audio_tv", "asw y", to_on_off_bool, false)
add_output("audio_aux", "asw z", to_on_off_bool, false)
add_output("amp_J"   , "outlets 4", to_on_off_bool, false)
add_output("amp_T"   , "outlets 6", to_on_off_bool, false)
add_output("screen_L", "outlets 2", to_on_off_bool, false)
add_output("screen_M", "outlets 1", to_on_off_bool, false)
add_output("screen_T", "outlets 3", to_on_off_bool, false)
add_output("receiver", "outlets 7", to_on_off_bool, false)
