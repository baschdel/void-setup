# Usage: <devdir> <pin> <class>

mkdir -p "$1"
cd "$1"
echo "$3" > class
PIN="$2"

script_base () {
	echo "#!/bin/bash"
	echo "GPIO_PIN=$PIN"
	echo "echo 20 text/plain"
}

on_script () {
	script_base
	echo 'raspi-gpio set $GPIO_PIN op pn dh > /dev/null'
	echo 'echo on'
}

off_script () {
	script_base
	echo 'raspi-gpio set $GPIO_PIN op pn dl > /dev/null'
	echo 'echo off'
}

status_script () {
	script_base
	echo 'raspi-gpio get $GPIO_PIN | grep -oE "level=([0-9]+)" | sed s/level=0/off/ | sed s/level=1/on/'
}

on_script > on
off_script > off
status_script > state

chmod +x on
chmod +x off
chmod +x state

